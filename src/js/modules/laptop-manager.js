import Laptop from "./laptop.js"
import Jumbotron from "./jumbotron.js"

export default class LaptopManager {
  /**
   * Used for handling the laptop select view and updating the jumbotron.
   * @param {Array<Laptop>} laptops 
   */
  constructor(laptops) {
    this.laptops = laptops
    this.selectedLaptop

    this.laptopSelect = document.getElementById('laptop-select')
    this.buyButton = document.getElementById('buy-button')
    this.laptopFeatures = document.getElementById('laptop-features')

    this.jumbotron = new Jumbotron()

    this.addLaptopsToSelectElement()
    this.jumbotron.setLaptop(this.selectedLaptop)
  }

  // Iterates the laptops-array and adds <option> elements to the <select> element
  addLaptopsToSelectElement() {
    this.laptops.forEach(laptop => {
      const option = document.createElement('option')

      option.setAttribute('value', laptop.name)
      option.innerText = laptop.name

      this.laptopSelect.appendChild(option)
    })

    // Add features text
    this.selectedLaptop = this.laptops[0]
    this.laptopFeatures.innerText = this.selectedLaptop.features
  }

  // Update the information on the site accoring to the newly selected laptop
  setSelectedLaptop(selectedLaptopName) {
      // Find the selected laptop and update the features text
      for (let i = 0; i < this.laptops.length; i++) {
        const laptop = this.laptops[i]

        if (laptop.name === selectedLaptopName) {
          this.laptopFeatures.innerText = laptop.features
          this.selectedLaptop = laptop
        }
      }

      // Update the jumbotron to match 
      this.jumbotron.setLaptop(this.selectedLaptop)
  }

}