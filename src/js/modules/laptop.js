export default class Laptop {
  constructor(name, features, price, imageURL) {
    this.name = name
    this.features = features
    this.price = price
    this.imageURL = imageURL
  }
}