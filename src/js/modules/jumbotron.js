/**
 * This class is responsible for updating the jumbotron. It is used by LaptopManager.
 */

export default class Jumbotron {
  constructor() {
    this.selectedTitle = document.getElementById('selected-title')
    this.selectedFeatures = document.getElementById('selected-features')
    this.selectedPrice = document.getElementById('selected-price')
    this.laptopImage = document.getElementById('laptop-image')
  }

  /**
   * Set the selected product view to the input laptop.
   * @param {Laptop} laptop 
   */
  setLaptop(laptop) {
    this.selectedTitle.innerText = laptop.name
    this.selectedFeatures.innerText = laptop.features
    this.laptopImage.setAttribute('src', laptop.imageURL)

    this.selectedPrice.innerText = `Price: ${laptop.price} kr`
    this.selectedPrice.setAttribute("data-price", laptop.price) // Include an attribute containing the price only
  }
}