export default class Bank {
  constructor() {
    this.balance = 0
    this.activeLoan = false

    this.balanceText = document.getElementById('bank-balance')
  }

  /**
   * Attempts to take a loan of the input value and returns an object containing
   * a message and a boolean value representing loan success or fail.
   * @param {Object} value 
   */
  getLoan(value) {
    let loanResult = { accepted: false, message: ""}

    if (this.activeLoan) {
      loanResult.message = 'You cannot apply for another loan until you buy another laptop!'
    }
    else if (value > this.balance * 2) {
      loanResult.message =  'You can not apply for a loan that is more than double your current balance!'
    }
    else {
      this.activeLoan = true
      this.addBalance(Number(value))

      loanResult.accepted = true
      loanResult.message = 'Loan granted. The money has been transfered to your account.'
    }

    return loanResult
  }

  addBalance(value) {
    this.balance += value
    this.balanceText.innerText = `Balance: ${this.balance} kr`
  }

  removeBalance(value) {
    this.balance -= value
    this.balanceText.innerText = `Balance: ${this.balance} kr`
  }

  hasEnoughBalance(value) {
    return value <= this.balance
  }
}
