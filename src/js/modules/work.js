
const PAYMENT_AMOUNT = 100  // The payment for working

// Takes a bank class as parameter and stores it as property
export default class Work {
  constructor() {
    this.payBalance = 0

    this.bankButton = document.getElementById('work-bank-button')
    this.workButton = document.getElementById('work-button')
    this.balanceText = document.getElementById('work-balance')
  }

  // Increase the pay balance and update text in HTML
  doWork() {
    this.payBalance += PAYMENT_AMOUNT
    this.balanceText.innerText = `Pay: ${this.payBalance} kr`
  }

  clearBalance() {
    this.payBalance = 0
    this.balanceText.innerText = `Pay: ${this.payBalance} kr`
  }
}
