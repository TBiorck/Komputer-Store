/**
 * The exported functions in this file are used by the EventManager to display messages
 * upon purchase attempts.
 */

const success = document.getElementById('success-alert')
const error = document.getElementById('error-alert')

export function displaySuccess(message) {
  hideAlerts()
  success.innerText = message
  showAlert(success)
}

export function displayError(message) {
  hideAlerts()
  error.innerText = message
  showAlert(error)
}

function hideAlerts() {
  success.hidden = true
  error.hidden = true
}

function showAlert(alert) {
  alert.hidden = false
}
