import Work from "../modules/work.js"
import Bank from "../modules/bank.js"
import LaptopManager from "../modules/laptop-manager.js"
import { displaySuccess, displayError } from './alert.js'

export default class EventManager {
  /**
   * This class is responsible for setting up the various eventlisteners required for the site.
   * @param {Bank} bank A Bank object.
   * @param {Work} work A Work object.
   * @param {LaptopManager} laptopManager A LaptopManager object.
   */
  constructor(bank, work, laptopManager) {
    this.bank = bank
    this.work = work
    this.laptopManager = laptopManager

    this.setWorkButtonListeners()
    this.setLaptopManagerListeners()
    this.setBankListeners()
  }

  /* WORK */
  setWorkButtonListeners() {
    // Clicking the work button call the doWork method, adding money to the work balance
    const workButton = document.getElementById('work-button')
    workButton.addEventListener('click', () => {
      this.work.doWork()  
    })
    
    // Clicking the bank button add the current work balance to the bank, then clears the work value
    const bankButton = document.getElementById('work-bank-button')
    bankButton.addEventListener('click', () => {
      this.bank.addBalance(this.work.payBalance)

      this.work.clearBalance()
    })
  }

  /* LAPTOP-MANAGER */
  setLaptopManagerListeners() {
    const laptopSelect = document.getElementById('laptop-select')
    
    // Update the features text when a new laptop is selected
    laptopSelect.addEventListener('change', event => {
      const selectedLaptopName = event.target.value

      this.laptopManager.setSelectedLaptop(selectedLaptopName)
    })
  }

  /* BANK */

  setBankListeners() {
    // Buy button
    const buyButton = document.getElementById('buy-button')
    
    buyButton.addEventListener('click', () => {
      const price = document.getElementById('selected-price').getAttribute('data-price')
      
      if (this.bank.hasEnoughBalance(price)) {
        this.bank.removeBalance(price)
        this.bank.activeLoan = false

        displaySuccess('You successfully bought a laptop. Money has been withdrawn from your account.')
      }
      else {
        displayError('You do not have sufficient funds to buy this product.')
      }
    })

    // Apply for loan button
    const applyLoanButton = document.getElementById('applyLoan-button')
    applyLoanButton.addEventListener('click', () => {
      const inputLoan = document.getElementById('input-loan').value

      const loanResult = this.bank.getLoan(inputLoan)
      if (loanResult.accepted) {
        displaySuccess(loanResult.message)
      }
      else {
        displayError(loanResult.message)
      }
    })
  }
}
