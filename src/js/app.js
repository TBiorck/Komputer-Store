import Bank from './modules/bank.js'
import Work from './modules/work.js'
import Laptop from './modules/laptop.js'
import LaptopManager from './modules/laptop-manager.js'
import EventManager from './util/event-manager.js'

// Create laptops,store in array
const laptop1 = new Laptop("Lenovo", "It is so good", 500, 'https://cdn1.vectorstock.com/i/1000x1000/45/70/laptop-icon-vector-2944570.jpg')
const laptop2 = new Laptop("Acer", "Good speed", 400, 'http://icons.iconarchive.com/icons/custom-icon-design/flatastic-7/512/Laptop-icon.png')
const laptop3 = new Laptop("Asus", "It is technically a computer!", 200, 'http://icons.iconarchive.com/icons/media-design/hydropro-v2/512/Laptop-icon.png')
const laptop4 = new Laptop('HP', 'You need this computer! It has a processor.', 1000, 'https://static.vecteezy.com/system/resources/previews/000/636/084/original/vector-laptop-icon-on-white-background.jpg')

let laptops = [laptop1, laptop2, laptop3, laptop4]

// Create module classes
const bank = new Bank()
const work = new Work()
const laptopManager = new LaptopManager(laptops)
const eventManager = new EventManager(bank, work, laptopManager)
