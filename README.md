# Komputer Store
This is the repository for the JavaScript task "The Komputer Store".

## Info
App.js is the main file which instantiates the other modules.

### Modules
The files in the src/js/modules directory contains separate funcitonality for the different components visible on the index page.

### Util
The files in the src/js/util directory contains helper functions. The alert.js is responsible for setting and showing/hiding alerts on different events. The EventManager sets eventlisteners for different components on the site.